//
//  MainCoordinator.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation
import FeedKit

class MainCoordinator: AppCoordinator {
    
    func presentCharacterDetail(_ details: CharacterDetailsViewModel) {
        guard let nav = navigationController else { return }
        
        let vc = CharacterDetailsViewController()
        vc.viewModel = details
        
        nav.pushViewController(vc, animated: true)
    }
}
