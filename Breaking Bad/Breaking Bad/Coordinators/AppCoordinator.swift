//
//  AppCoordinator.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation
import UIKit

class AppCoordinator: RootCoordinator {
    
    var window: UIWindow?
    
    override func start() {
        showMainScreen()
    }
    
    func styleNavigation() {
        let barAppearance = UINavigationBarAppearance()
        barAppearance.backgroundColor = .white
        navigationController?.navigationBar.scrollEdgeAppearance = barAppearance
        navigationController?.navigationBar.standardAppearance = barAppearance
        navigationController?.navigationBar.barTintColor = .white
    }
    
    func showMainScreen() {
        styleNavigation()
        guard let nav = navigationController else { return }
        let vc = HomeViewController()
        
        vc.coordinator = MainCoordinator(navigationController: nav)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .fade
        nav.view.layer.add(transition, forKey: nil)
        nav.pushViewController(vc, animated: false)
    }
}
