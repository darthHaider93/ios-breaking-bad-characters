//
//  CharacterService.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation
import PromiseKit

protocol CharacterServiceProtocol {
    func getCharacterDetails() -> Promise<[Character]>
}

class CharacterService: CharacterServiceProtocol {
    
    public func getCharacterDetails() -> Promise<[Character]> {
  
        return Promise() { resolver in
            NetworkManager.default.request([Character].self, method: .get, completion: { response in
                switch(response) {
                case .success(let characters):
//                    let a = Array.init(arrayLiteral: characters)
                    resolver.fulfill(characters)
                case .failure(let error):
                    resolver.reject(error)
                }
            })
        }
    }
}
