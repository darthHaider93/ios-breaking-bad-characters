//
//  HomeViewCollectionCell.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation
import UIKit
import Kingfisher

class HomeViewCollectionCell: UICollectionViewCell {
    
    private lazy var imageView: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "placeholder")
        i.contentMode = .scaleAspectFill
        i.layer.cornerRadius = 5
        i.layer.cornerCurve = .continuous
        i.clipsToBounds = true
        i.layer.borderWidth = 2.0
        i.layer.borderColor = UIColor.white.cgColor
        return i
    }()
    
    private lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 18)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .left
        return l
    }()
    
    private let imageHeight: CGFloat = 190.0
    
    var imageUrlString: String? {
        didSet {
            guard let urlString = imageUrlString, let url = URL(string: urlString) else {
                imageView.image = nil
                return
            }
            
            imageView.kf.setImage (with: url, options: [.forceTransition, .transition(.fade(0.2))])
        }
    }
    
    var name: String? {
        didSet {
            guard let n = self.name else { return }
            titleLabel.text = n
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configData(data: HomeCharacterDetail) {
        self.imageUrlString = data.image
        self.name = data.name
    }
    
    func setup() {
        
        contentView.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.heightAnchor.constraint(equalToConstant: imageHeight),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
        ])
    }
}
