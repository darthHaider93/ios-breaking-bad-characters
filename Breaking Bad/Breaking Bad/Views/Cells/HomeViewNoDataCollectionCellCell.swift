//
//  HomeViewNoDataCollectionCellCell.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation
import UIKit

class HomeViewNoDataCollectionCellCell: UICollectionViewCell {
        
    private lazy var imageView: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "placeholder")
        i.contentMode = .scaleAspectFill
        i.layer.cornerRadius = 5
        i.layer.cornerCurve = .continuous
        i.clipsToBounds = true
        i.layer.borderWidth = 2.0
        i.layer.borderColor = UIColor.white.cgColor
        return i
    }()
            
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        add(imageView)
        imageView.pinToParentView()
    }
}
