//
//  HomeViewController.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation
import WebKit

class HomeViewController: ViewController {
    
    var coordinator: MainCoordinator?
    
    var viewModel = HomeViewModel()
    
    private lazy var refreshControl: UIRefreshControl = {
        let r = UIRefreshControl()
        r.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        r.tintColor = .blue
        return r
    }()
    
    private lazy var collectionView: UICollectionView = {
        let viewLayout = UICollectionViewFlowLayout()
        viewLayout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: viewLayout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsHorizontalScrollIndicator = false
        HomeViewCollectionCell.register(in: cv)
        HomeViewNoDataCollectionCellCell.register(in: cv)
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.keyboardDismissMode = .onDrag
        return cv
    }()
    
    private lazy var searchBar: UISearchBar = {
        let searchBar:UISearchBar = UISearchBar()
        searchBar.searchBarStyle = .default
        searchBar.searchTextField.backgroundColor = .white
        searchBar.returnKeyType = .search
        searchBar.backgroundImage = UIImage.imageWithColor(color: .black)
        searchBar.placeholder = "Search Character..." //localise
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    
    private lazy var navTitleStackView: UIStackView = {
        let s = UIStackView(arrangedSubviews: [navTitleViewImage])
        s.axis = .horizontal
        s.alignment = .center
        s.spacing = 6
        return s
    }()
    
    private lazy var navTitleViewImage: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "nav logo")
        i.contentMode = .scaleAspectFit
        i.clipsToBounds = true
        return i
    }()
    
    private lazy var numberOfResultsLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Number of results = 0" //localise this!
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = .green
        l.numberOfLines = 0
        l.textAlignment = .center
        return l
    }()
    
    
    private enum LayoutConstant {
        static let spacing: CGFloat = 10.0
        static let lineSpacing: CGFloat = 16.0
        static let itemHeight: CGFloat = 255
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getCharacters()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildViews()
        bindViewModel()
        searchBar.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func configureNavBar() {
        navigationItem.titleView = navTitleStackView
        
        let filterButton = UIBarButtonItem(image: UIImage(named: "filter icon")?.withRenderingMode(.alwaysOriginal),
                                           style: .plain,
                                           target: self,
                                           action: #selector(showFilters))
        navigationItem.rightBarButtonItem = filterButton
    }
    
    private func buildViews() {
        view.backgroundColor = .black
        configureNavBar()
        
        collectionView.refreshControl = refreshControl
        
        view.add(searchBar, numberOfResultsLabel, collectionView)
        
        NSLayoutConstraint.activate([
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            numberOfResultsLabel.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 10),
            numberOfResultsLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            collectionView.topAnchor.constraint(equalTo: numberOfResultsLabel.bottomAnchor, constant: 16),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16)
        ])
    }
    
    func bindViewModel() {
        viewModel.addAction(for: self, event: .error) {
            [weak self] (error: Error) in
            self?.coordinator?.presentErrorMessage(error.localizedDescription)
        }
        
        viewModel.addAction(for: self, event: .charactersUpdated) {
            [weak self] (resultsCount: Int) in
            self?.numberOfResultsLabel.text = "Number of results = \(resultsCount)"
            self?.collectionView.reloadData()
        }
        
        viewModel.addAction(for: self, event: .networkActivity) { [weak self] (isActive: Bool) in
            guard !isActive else {
                self?.showLoadingSpinner()
                return
            }
            
            self?.hideLoadingSpinner()
            self?.refreshControl.endRefreshing()
        }
        
        viewModel.addAction(for: self, event: .filterChanged) { [weak self] (filterApplied: Int?) in
            
            self?.navigationItem.rightBarButtonItem?.image = filterApplied == nil ? UIImage(named: "filter icon")?.withRenderingMode(.alwaysOriginal) : UIImage(named: "filter applied icon")?.withRenderingMode(.alwaysOriginal)
        }        
    }
    
    func itemWidth(for width: CGFloat, spacing: CGFloat) -> CGFloat {
        let itemsInRow: CGFloat = 2
        let totalSpacing: CGFloat = 4 * spacing + (itemsInRow - 1) * spacing
        let finalWidth = (width - totalSpacing) / itemsInRow
        return floor(finalWidth)
    }
    
    private lazy var loadingSpinner = SpinnerViewController()
    
    func showLoadingSpinner() {
        addChild(loadingSpinner)
        loadingSpinner.view.frame = view.frame
        view.addSubview(loadingSpinner.view)
        loadingSpinner.didMove(toParent: self)
    }
    
    func hideLoadingSpinner() {
        loadingSpinner.willMove(toParent: nil)
        loadingSpinner.view.removeFromSuperview()
        loadingSpinner.removeFromParent()
    }
    
    // MARK: - Actions
    
    @objc func reloadData() {
        viewModel.getCharacters()
    }
    
    @objc func showFilters() {        
        guard let nav = navigationController else { return }
        let vm = FiltersViewModel()
        vm.seasonFilterApplied = self.viewModel.seasonFilterApplied
        
        let vc = FiltersViewController()
        vc.viewModel = vm
        
        vc.seasonToFilterBy = { [weak self] season in
            self?.viewModel.filterCharactersBySeason(season)
        }
        
        vc.clearFilter = { [weak self] in
            self?.viewModel.seasonFilterApplied = nil
            self?.viewModel.getCharacters()
        }
        
        nav.modalPresentationStyle = .overFullScreen
        nav.present(vc, animated: true, completion: nil)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.characters.isEmpty ? 0 : self.viewModel.characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = HomeViewCollectionCell.dequeue(from: collectionView, for: indexPath)
        cell.configData(data: viewModel.getDataAtIndex(index: indexPath.row))
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        debugPrint("Item at index: \(indexPath.row) tapped")
        coordinator?.presentCharacterDetail(viewModel.getFullCharacterDetailAtIndex(index: indexPath.row))
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = itemWidth(for: view.frame.width-16, spacing: LayoutConstant.spacing)
        return CGSize(width: width, height: LayoutConstant.itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: LayoutConstant.spacing, left: LayoutConstant.spacing*2, bottom: LayoutConstant.spacing, right: LayoutConstant.spacing*2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return LayoutConstant.spacing
    }
}

extension HomeViewController: UISearchBarDelegate {
    //add some check here with timer to ensure api not hammered constantly! - Future enhancement
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchCharacter(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        viewModel.searchCharacter(text)
    }
}
