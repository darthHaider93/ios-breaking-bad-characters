//
//  FiltersViewController.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 24/09/2021.
//

import Foundation
import UIKit

class FiltersViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    private lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Filter By Season Appearance" //localise this!
        l.font = UIFont.systemFont(ofSize: 22)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .center
        return l
    }()
    
    private lazy var filterAppliedLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.text = "Current Season Filter Applied is: NONE" //localise this!
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = .green
        l.numberOfLines = 0
        l.textAlignment = .center
        return l
    }()
    
    private lazy var filterButton: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Choose Season", for: .normal) //localise this!
        b.setBackgroundImage(UIImage.imageWithColor(color: .white), for: .normal)
        b.setTitleColor(.black, for: .normal)
        b.heightAnchor.constraint(equalToConstant: 40).isActive = true
        b.addTarget(self, action: #selector(showAlert), for: .touchUpInside)
        return b
    }()
    
    private lazy var clearFilterButton: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Clear Filter", for: .normal) //localise this!
        b.setBackgroundImage(UIImage.imageWithColor(color: .red), for: .normal)
        b.setTitleColor(.black, for: .normal)
        b.heightAnchor.constraint(equalToConstant: 40).isActive = true
        b.addTarget(self, action: #selector(clear), for: .touchUpInside)
        return b
    }()
    
    private lazy var pickerView: UIPickerView = {
        let p = UIPickerView()
        p.translatesAutoresizingMaskIntoConstraints = false
        return p
    }()
        
    var seasonToFilterBy: ((Int) -> Void)?
    var clearFilter: (() -> Void)?
    
    var viewModel = FiltersViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildViews()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
    }
    
    func buildViews() {
        view.backgroundColor = .black
        
        view.add(titleLabel, filterAppliedLabel, filterButton, clearFilterButton)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            filterAppliedLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            filterAppliedLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            filterAppliedLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            filterButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            filterButton.bottomAnchor.constraint(equalTo: clearFilterButton.topAnchor, constant: -20),
            filterButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            clearFilterButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            clearFilterButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
            clearFilterButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
        ])
        
        if let filteredSeason = viewModel.seasonFilterApplied {
            self.filterAppliedLabel.text =  "Current Season Filter Applied is SEASON: \(filteredSeason)"
        }
    }
    
    
    // MARK: - Actions
    
    @objc func clear() {
        self.dismiss(animated: true) {
            self.viewModel.seasonFilterApplied = nil
            self.clearFilter?()
        }
    }
    
    @objc func showAlert() { //localise
        let ac = UIAlertController(title: "Picker", message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        ac.view.addSubview(pickerView)
        
        NSLayoutConstraint.activate([
            pickerView.heightAnchor.constraint(equalToConstant: 150),
            pickerView.widthAnchor.constraint(equalToConstant: 250),
            pickerView.centerYAnchor.constraint(equalTo: ac.view.centerYAnchor),
            pickerView.centerXAnchor.constraint(equalTo: ac.view.centerXAnchor),
        ])
        
        ac.addAction(UIAlertAction(title: "OK".localised(), style: .default, handler: { _ in
            let pickerValue = self.viewModel.pickerData[self.pickerView.selectedRow(inComponent: 0)]
            
            self.dismiss(animated: true) {
                self.seasonToFilterBy?(pickerValue)
            }
        
        }))
        ac.addAction(UIAlertAction(title: "CANCEL".localised(), style: .cancel, handler: nil))
        present(ac, animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(viewModel.pickerData[row])"
    }
}
