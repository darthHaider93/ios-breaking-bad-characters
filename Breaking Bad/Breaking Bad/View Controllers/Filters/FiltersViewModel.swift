//
//  FiltersViewModel.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 24/09/2021.
//

import PromiseKit
import FeedKit

class FiltersViewModel {
    
    var seasonFilterApplied: Int?
    
    var pickerData = [Int]()
    
    private let minNum = 1
    private let maxNum = 6
    
    init() {
        pickerData = Array(stride(from: minNum, to: maxNum + 1, by: 1))
    }
    
}
