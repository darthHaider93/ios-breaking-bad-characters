//
//  CharacterDetailsViewController.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 24/09/2021.
//

import Foundation
import UIKit

class CharacterDetailsViewController: ViewController {
    
    private lazy var imageView: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "placeholder")
        i.contentMode = .scaleAspectFill
        i.layer.cornerRadius = 5
        i.layer.cornerCurve = .continuous
        i.clipsToBounds = true
        i.layer.borderWidth = 2.0
        i.layer.borderColor = UIColor.white.cgColor
        return i
    }()
    
    private lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 18)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .left
        return l
    }()
    
    private lazy var occupationLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 18)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .left
        return l
    }()
    
    private lazy var statusLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 18)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .left
        return l
    }()
    
    private lazy var nickNameLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 18)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .left
        return l
    }()
    
    private lazy var appearanceLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 18)
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .left
        return l
    }()
    
    var imageUrlString: String? {
        didSet {
            guard let urlString = imageUrlString, let url = URL(string: urlString) else {
                imageView.image = nil
                return
            }
            
            imageView.kf.setImage (with: url, options: [.forceTransition, .transition(.fade(0.2))])
        }
    }
    
    var viewModel = CharacterDetailsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        buildViews()
    }
    
    func buildViews() {
        view.backgroundColor = .black
        
        view.add(imageView, titleLabel, occupationLabel, statusLabel, nickNameLabel, appearanceLabel)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1.2),
            
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            occupationLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            occupationLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            occupationLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            statusLabel.topAnchor.constraint(equalTo: occupationLabel.bottomAnchor, constant: 10),
            statusLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            statusLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            nickNameLabel.topAnchor.constraint(equalTo: statusLabel.bottomAnchor, constant: 10),
            nickNameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            nickNameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),

            appearanceLabel.topAnchor.constraint(equalTo: nickNameLabel.bottomAnchor, constant: 10),
            appearanceLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            appearanceLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
        ])
        
        setData()
    }
    
    func setData() {
        if let name = viewModel.name {
            titleLabel.text = "Name: \(name)"
            self.title = name
        }
        
        if let occ = viewModel.occupation {
            occupationLabel.text = "Occupation: \(occ)"
        }
        
        if let status = viewModel.status {
            statusLabel.text = "Status: \(status)"
        }
        
        if let nickname = viewModel.nickname {
            nickNameLabel.text = "Nickname: \(nickname)"
        }
        
        if let appearances = viewModel.seasonAppearance {
            appearanceLabel.text = "Appears in seasons: \(appearances)"
        }
        
        imageUrlString = viewModel.image
    }
}
