//
//  CharacterDetailsViewModel.swift.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 24/09/2021.
//

import Foundation

struct CharacterDetailsViewModel {
    var name: String?
    var image: String?
    var occupation: [String]?
    var status: String?
    var nickname: String?
    var seasonAppearance: [Int]?
}
    
