//
//  SpinnerViewController.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 24/09/2021.
//

import Foundation
import UIKit

class SpinnerViewController: UIViewController {
    
    private lazy var spinner: UIActivityIndicatorView = {
        let s = UIActivityIndicatorView(style: .large)
        s.color = .green
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
