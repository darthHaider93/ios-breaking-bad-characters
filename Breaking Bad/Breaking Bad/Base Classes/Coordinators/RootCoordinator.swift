//
//  RootCoordinator.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import UIKit

class RootCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    
    weak var navigationController: UINavigationController?
    
    var rootViewController: UIViewController? {
        navigationController?.viewControllers.first
    }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        
    }
    
}
