//
//  Coordinated.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation
import UIKit

protocol Coordinated {
    var coordinator: RootCoordinator? { get set }
}
