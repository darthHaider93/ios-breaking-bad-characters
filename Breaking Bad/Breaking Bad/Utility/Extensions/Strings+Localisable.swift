//
//  Strings+Localisable.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation

extension String {
    
    func localised() -> String {
        return NSLocalizedString(self, comment: self)
    }
}
