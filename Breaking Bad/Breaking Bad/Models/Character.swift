//
//  Character.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 22/09/2021.
//

import Foundation

public struct Character: Codable {
    // MARK: Properties
    public var id: Int?
    public var name: String?
    public var birthday: String?
    public var status: String?
    public var nickname: String?
    public var image: String?
    public var portrayed: String?
    public var category: String?
    public var occupation: [String]?
    public var appearance: [Int]?
    public var better_call_saul_appearance: [Int]?
}

extension Character {
    enum CodingKeys: String, CodingKey {
        case id = "char_id"
        case image = "img"
        case name = "name"
        case birthday = "birthday"
        case status = "status"
        case nickname = "nickname"
        case portrayed = "portrayed"
        case category = "category"
        case occupation = "occupation"
        case appearance = "appearance"
        case better_call_saul_appearance = "better_call_saul_appearance"
    }
}




