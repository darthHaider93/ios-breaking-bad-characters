//
//  HomeCharacterDetail.swift
//  Breaking Bad
//
//  Created by Haider Ashfaq on 23/09/2021.
//

import Foundation

struct HomeCharacterDetail {
    var name: String?
    var image: String?
}
