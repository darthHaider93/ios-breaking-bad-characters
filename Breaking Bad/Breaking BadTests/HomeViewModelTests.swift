//
//  HomeViewModelTests.swift
//  Breaking BadTests
//
//  Created by Haider Ashfaq on 24/09/2021.
//

import Foundation
import XCTest
import PromiseKit
@testable import Breaking_Bad

class HomeViewModelTests: XCTestCase {
    
    var sut: HomeViewModel!
    var mockService: MockCharacterServiceService!
    
    let character1 = Character(id: 1, name: "Test1", birthday: "TestBirthday", status: nil, nickname: "TestNickname", image: nil, portrayed: nil, category: nil, occupation: nil, appearance: nil, better_call_saul_appearance: nil)
    
    let character2 = Character(id: 1, name: "Test2", birthday: "TestBirthday2", status: nil, nickname: "TestNickname2", image: nil, portrayed: nil, category: nil, occupation: nil, appearance: [1], better_call_saul_appearance: nil)

    override func setUp() {
        super.setUp()
        mockService = MockCharacterServiceService()
        sut = HomeViewModel(service: mockService)
    }
    
    override func tearDown() {
        sut = nil
        mockService = nil
        super.tearDown()
    }
    
    func testGetCharacterDetails() {
        _ = mockService.getCharacterDetails()
    
        // Assert
        XCTAssert(mockService.isGetCharacterDetailsCalled)
    }
    
    func testHomeViewModelInitCharacters() {
        sut.getCharacters()
        XCTAssertNotNil(sut.characters)
 
        //test more properties in future
    }
    
    func testSearchFunctionalityWorksToFindMatch() {
        sut.characters = [character1, character2]
        sut.searchCharacter("Test1")
        XCTAssertEqual(sut.characters.count, 1)
        XCTAssertEqual(sut.characters[0].name, "Test1")
        XCTAssertEqual(sut.characters[0].birthday, "TestBirthday")
    }
    
    func testFilterByAppearanceFunctionalityWorksWhenMatch() {
        sut.characters = [character1, character2]
        sut.filterCharactersBySeason(1)
        XCTAssertEqual(sut.characters.count, 1)
        XCTAssertEqual(sut.characters[0].name, "Test2")
    }
    
    func testFilterByAppearanceFunctionalityWorksWhenNoMatch() {
        sut.characters = [character1, character2]
        sut.filterCharactersBySeason(4)
        XCTAssertEqual(sut.characters.count, 0)
    }
    
    func testGetDataAtIndex() {
        sut.characters = [character1, character2]
        let c = sut.getDataAtIndex(index: 0)
        XCTAssertEqual(c.name, "Test1")
        XCTAssertEqual(c.image, nil)
        
        let c2 = sut.getDataAtIndex(index: 1)
        XCTAssertEqual(c2.name, "Test2")
        XCTAssertEqual(c2.image, nil)
    }
}


class MockCharacterServiceService: CharacterServiceProtocol {
    
    let character1 = Character(id: 1, name: "Test1", birthday: "TestBirthday", status: nil, nickname: "TestNickname", image: nil, portrayed: nil, category: nil, occupation: nil, appearance: nil, better_call_saul_appearance: nil)
    
    var isGetCharacterDetailsCalled = false
    
    func getCharacterDetails() -> Promise<[Character]> {
        isGetCharacterDetailsCalled = true
        
        return Promise() { resolver in
            resolver.fulfill([character1])
        }
    }
}
